//Using Sequelize (ORM for Postgress)
const Sequelize = require('sequelize');
const config = require('config');

const host = config.get('db.host');
const database = config.get('db.database');
const username = config.get('db.user');
const password = config.get('db.password');
const dbPort = config.get('db.port');

module.exports = new Sequelize(database, username, password, {
    host: host,
    dialect: 'postgres',
    port: dbPort
}); 
