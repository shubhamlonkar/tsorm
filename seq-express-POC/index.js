const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const route = require('./src/routes/routes')
const sequelize = require('./config/sequelize');
const userProfile = require('./src/models/models')
const app = express();

app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/', route);

sequelize.sync().then(() => {
    console.log('Synced Models');
});


module.exports = app;

