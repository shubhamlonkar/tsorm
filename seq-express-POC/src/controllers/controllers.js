'use strict'

const models = require('../models/models');
const { Validator } = require('node-input-validator');

const v = new Validator(
    {
        'name': 'required',
        'startDate': 'required',
        'description': 'required',
        'projectstatus': 'required',
        'projecttype': 'required',
        'BU': 'required'
    },
);



exports.addProject = async function (request, response) {
    console.log("request.body", request.body);
    let result;

    /* models.project.findOrCreate({
        where: { name: request.body.name },
        defaults: {
            startDate: request.body.startDate,
            description: request.body.description,
            projectStatus: request.body.projectstatus,
            projectType: request.body.projecttype,
            infraCost: request.body.infraCost,
            BU: request.body.BU
        }
    }).then((result) => {
        console.log(result);
        let [Project, wasCreated] = result;
        console.log("wasCreated", wasCreated);
        let memberArray = [];
        console.log(Project);
        request.body.members.forEach((element) => {
            models.member.create({
                name: element
            }).then((memberObj) => {
                memberArray.push(memberObj);
                Project.addMember(memberArray);
                
            });
        });
        return response.send("Success");
    }) */

    result = await models.project.findOrCreate({
        where: { name: request.body.name },
        defaults: {
            startDate: request.body.startDate,
            description: request.body.description,
            projectStatus: request.body.projectstatus,
            projectType: request.body.projecttype,
            infraCost: request.body.infraCost,
            BU: request.body.BU
        }
    });
    //console.log("result===>",result);
    let [Project, wasCreated] = result;
    let memberArray = [];
    //console.log("Project",Project);
    var i;

    for (i of request.body.members) {
        let memberObj = await models.member.create({
            name: i
        })
        console.log("memberObj####################", memberObj);
        memberArray.push(memberObj);
        console.log("memberArray=========>", memberArray);
        let output = await Project.addMember(memberArray);
        console.log("output==============>", output);

    }




    /* let memberObj = await models.member.create({
        name: request.body.members[0]
    })
        console.log("memberObj####################", memberObj);
        memberArray.push(memberObj);
        console.log("memberArray=========>",memberArray);
       let output =  await Project.addMember(memberArray);
        console.log("output==============>",output);
 */
    response.send("End");
}