'use strict';
const express = require('express');
const controllers = require('../controllers/controllers');
const bodyParser = require('body-parser');
const app = express();


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//TODO: Allow cross origin only for authorized users
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.post('/add-project',controllers.addProject);

 module.exports = app;
 
 



