const sequelizeConf = require('../../config/sequelize');

const Sequelize = require('sequelize');


const project = sequelizeConf.define('Project', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true // Automatically gets converted to SERIAL for postgres
    },

    name: {
        type: Sequelize.STRING(100),
        allowNull: false
    },

    startDate: {
        type: Sequelize.DATEONLY,
        allowNull: false
    },

    betaReleaseDate: {
        type: Sequelize.DATEONLY,
        allowNull: true
        
    },

    releaseDate: {
        type: Sequelize.DATEONLY,
        allowNull: true
    },

    description: {
        type: Sequelize.STRING(500),
        allowNull: false
    },

    projectStatus: {
        type: Sequelize.ENUM,
        values: ['active', 'on hold', 'completed'],
        defaultValue: 'active'
    },

    projectType: {
        type: Sequelize.ENUM,
        values: ['internal', 'external'],
        defaultValue: 'internal'
    },

    frontEndGitlabRepo: {
        type: Sequelize.STRING(200),
        allowNull: true
    },

    backEndGitlabRepo: {
        type: Sequelize.STRING(200),
        allowNull: true
    },

    infraCost: {
        type: Sequelize.INTEGER,
        allowNull: true
    },

    BU: {
        type: Sequelize.STRING(200),
        allowNull: false
    }

});

const member = sequelizeConf.define('Member', {
    employeeID: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name: {
        type: Sequelize.STRING(100),
        allowNull: false
    },
    startDate: {
        type: Sequelize.DATEONLY,
        allowNull: true
    },
    endDate: {
        type: Sequelize.DATEONLY,
        allowNull: true
    },
    designation: {
        type: Sequelize.STRING(100),
        allowNull: true
    },
    billingCost: {
        type: Sequelize.INTEGER,
        allowNull: true
    }
});

project.hasMany(member);
member.belongsTo(project);

module.exports = {project,member};