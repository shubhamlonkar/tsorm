"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const routing_controllers_1 = require("routing-controllers");
const typeorm_1 = require("typeorm");
const User_1 = require("./entity/User");
let UserController = class UserController {
    /* @Get("/users")
    async getAll() {
       //return "This action returns all users";
       
       // get a post repository to perform operations with post
       const postRepository = getManager().getRepository(User);
 
       // load a post by a given post id
       const users = await postRepository.find();
 
       // return loaded posts
       return users;
 
    } */
    getAll(request, response) {
        return __awaiter(this, void 0, void 0, function* () {
            //return "This action returns all users";
            // get a post repository to perform operations with post
            console.log("Inside GetAll");
            const postRepository = typeorm_1.getManager().getRepository(User_1.User);
            // load a post by a given post id
            const users = yield postRepository.find();
            // return loaded posts
            //// return users;
            response.send(users);
        });
    }
    //@Get("/users/:id")
    getOne(id, response) {
        return __awaiter(this, void 0, void 0, function* () {
            //return "This action returns user #" + id;
            // get a post repository to perform operations with post
            const postRepository = typeorm_1.getManager().getRepository(User_1.User);
            // load a post by a given post id
            const users = yield postRepository.findOne(id);
            console.log("Users===>", users);
            // return loaded posts
            response.send(users);
        });
    }
    /* @Post("/users")
    async post(@Body() user: any) {
       const postRepository = getManager().getRepository(User);
 
     // create a real post object from post json object sent over http
     
     const newUser = postRepository.create(user);
 
     console.log("request body====>",user);
     // save received post
     await postRepository.save(newUser);
 
     return newUser;
 
    } */
    post(user, response) {
        return __awaiter(this, void 0, void 0, function* () {
            const postRepository = typeorm_1.getManager().getRepository(User_1.User);
            // create a real post object from post json object sent over http
            const newUser = postRepository.create(user.body);
            console.log("request body====>", user);
            // save received post
            yield postRepository.save(newUser);
            ////return newUser;
            response.send(newUser);
        });
    }
};
__decorate([
    __param(0, routing_controllers_1.Param("id")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "getOne", null);
__decorate([
    __param(0, routing_controllers_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UserController.prototype, "post", null);
UserController = __decorate([
    routing_controllers_1.JsonController()
], UserController);
exports.UserController = UserController;
