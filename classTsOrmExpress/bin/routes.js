"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const UserController_1 = require("./UserController");
let user = new UserController_1.UserController();
console.log("Inside routes");
exports.AppRoutes = [
    {
        path: "/users",
        method: "get",
        action: user.getAll
    },
    {
        path: "/users",
        method: "post",
        action: user.post
    },
    {
        path: "/users/:id",
        method: "get",
        action: user.getOne
    }
];
