"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
require("reflect-metadata");
const typeorm_1 = require("typeorm");
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const routes_1 = require("./routes");
typeorm_1.createConnection().then((connection) => __awaiter(void 0, void 0, void 0, function* () {
})).catch(error => console.log("TypeORM connection error: ", error));
// creates express app, registers all controller routes and returns you express app instance
const app = express_1.default();
app.use(body_parser_1.default.json());
//register all application routes
routes_1.AppRoutes.forEach(route => {
    console.log("Inside indexts");
    app[route.method](route.path, (request, response, next) => {
        route.action(request, response)
            .then(() => next)
            .catch(err => next(err));
    });
});
module.exports = app;
