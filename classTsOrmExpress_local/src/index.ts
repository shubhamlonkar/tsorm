import "reflect-metadata";
import { createConnection } from "typeorm";
import { User } from "./entity/User";
import { createExpressServer } from "routing-controllers";
//import {UserController} from "./UserController";
import { Request, Response } from "express";
import * as express from "express";
import * as bodyParser from "body-parser";
import { AppRoutes } from "./routes";


// creates express app, registers all controller routes and returns you express app instance
const app = express();
    app.use(bodyParser.json());

    //register all application routes
    AppRoutes.forEach(route => {
        console.log("Inside indexts");
        app[route.method](route.path, (request: Request, response: Response, next: Function) => {
            route.action(request, response)
                .then(() => next)
                .catch(err => next(err));
        });
    });

createConnection().then(async connection => {


}).catch(error => console.log("TypeORM connection error: ", error));


export default app;