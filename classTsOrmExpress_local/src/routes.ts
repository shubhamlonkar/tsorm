import {UserController } from "./UserController";


let user = new UserController();

console.log("Inside routes");

export const AppRoutes = [
    {
        path:"/users",
        method: "get",
        action: user.getAll
    },
    {
        path:"/users",
        method: "post",
        action: user.post
    }
]
