import { Controller, Param, Body, Get, Post, Put, Delete,JsonController } from "routing-controllers";
import { Request, Response } from "express";
import { getManager } from "typeorm";
import { User } from "./entity/User";

@JsonController()
export class UserController {

   /* @Get("/users")
   async getAll() {
      //return "This action returns all users";
      
      // get a post repository to perform operations with post
      const postRepository = getManager().getRepository(User);

      // load a post by a given post id
      const users = await postRepository.find();

      // return loaded posts
      return users;

   } */

   async getAll(request: Request,response: Response) {
      //return "This action returns all users";
      
      // get a post repository to perform operations with post
      console.log("Inside GetAll");
      const postRepository = getManager().getRepository(User);

      // load a post by a given post id
      const users = await postRepository.find();

      // return loaded posts
     //// return users;
     response.send(users);

   } 
   

   /* @Get("/users/:id")
   async getOne(@Param("id") id: number) {
      //return "This action returns user #" + id;


      // get a post repository to perform operations with post
      const postRepository = getManager().getRepository(User);

      // load a post by a given post id
      const users = await postRepository.findOne(id);

      // return loaded posts
      return users;

   } */

   /* @Post("/users")
   async post(@Body() user: any) {
      const postRepository = getManager().getRepository(User);

    // create a real post object from post json object sent over http
    
    const newUser = postRepository.create(user);

    console.log("request body====>",user);
    // save received post
    await postRepository.save(newUser);

    return newUser;

   } */


   async post(@Body() user: any,response:Response) {
      const postRepository = getManager().getRepository(User);

    // create a real post object from post json object sent over http
    
    const newUser = postRepository.create(user.body);

    console.log("request body====>",user);
    // save received post
    await postRepository.save(newUser);

    ////return newUser;
    response.send(newUser);

   }

   /* @Put("/users/:id")
   put(@Param("id") id: number, @Body() user: any) {
      return "Updating a user...";
   }

   @Delete("/users/:id")
   remove(@Param("id") id: number) {
      return "Removing user...";
   } */

}