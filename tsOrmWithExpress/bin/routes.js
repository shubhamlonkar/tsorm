"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const GetResponse_1 = require("./controller/GetResponse");
const PostResponse_1 = require("./controller/PostResponse");
exports.AppRoutes = [
    {
        path: "/users",
        method: "get",
        action: GetResponse_1.getUsers
    },
    {
        path: "/users",
        method: "post",
        action: PostResponse_1.storeUsers
    }
];
