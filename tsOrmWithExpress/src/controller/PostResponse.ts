import {Request,Response} from "express";
import {User} from "../entity/User";
import {getManager} from "typeorm";

export async function storeUsers(request:Request,response: Response){

    
    const postRepository = getManager().getRepository(User);

    // create a real post object from post json object sent over http
    const newUser = postRepository.create(request.body);

    // save received post
    await postRepository.save(newUser);

    // return saved post back
    response.send(newUser);
}