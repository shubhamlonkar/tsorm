import "reflect-metadata";
import {createConnection} from "typeorm";
import {Request,Response} from "express";
import  express from "express";
import bodyParser from "body-parser";
import {AppRoutes} from "./routes";
import {User} from "./entity/User";

 createConnection().then(async connection => {


}).catch(error => console.log("TypeORM connection error: ",error));
 
//create express app
const app = express();
app.use(bodyParser.json());

//register all application routes
AppRoutes.forEach(route => {
    app[route.method](route.path, (request: Request, response: Response, next:Function) => {
        route.action(request,response)
        .then(() => next)
        .catch(err => next(err));
    });
});


export = app;