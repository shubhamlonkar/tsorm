import {getUsers} from "./controller/GetResponse";
import {storeUsers} from "./controller/PostResponse";

export const AppRoutes = [
    {
        path:"/users",
        method: "get",
        action: getUsers
    },
    {
        path:"/users",
        method: "post",
        action: storeUsers
    }
]